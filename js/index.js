$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({interval: 1000});
    
    $('#contacto').on('show.bs.modal', function(e){
        console.log('abriendo modal...');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-secondary');
        $('#contactoBtn').prop('disable',true);
    });
    
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('modal abierto');    
    });
    
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('ocultando modal...');
        $('#contactoBtn').removeClass('btn-secondary');
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').prop('disable',false);
    });
    
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('modal oculto');
    });
});